class StringifiedNumberCalculator {

    constructor(initialNumStr) {
        this.value = initialNumStr;
    }
    
    __leftPadWithZeroes(str, padToLength) {
        for (let ii = 0; ii < padToLength; ii++) {
            str = "0" + str;
        }
        return str.substring(str.length - padToLength);
    }

    plus(numStr) {
        let carryOver = 0;

        let num1 = numStr.split("")
        let num2 = this.value.split("")

        let solution = new Array(num1.length + num2.length)

        for (let ii in num1) {
            for (let jj in num2) {
                let currentDigitIndex = solution.length - parseInt(ii) - parseInt(jj) - 1

                let sumOfDigits = num1[num1.length - 1 - ii] + num2[num2.length - 1 - jj]

                let newDigitAndCarryOver = (sumOfDigits + carryOver + (parseInt(solution[currentDigitIndex] || 0))).toString().split('').reverse()
                let newDigit = newDigitAndCarryOver[0]
                carryOver = parseInt(newDigitAndCarryOver[1] || 0)

                solution[currentDigitIndex] = newDigit;
            }
        }

        if (carryOver > 0) solution.unshift(carryOver + '')

        this.value = solution.join('');

        return this
    }

    minus(numStr) {
        let carryOver = 0;

        let num1 = numStr.split("")
        let num2 = this.value.split("")

        let solution = new Array(num1.length + num2.length)

        for (let ii in num1) {
            for (let jj in num2) {
                let currentDigitIndex = solution.length - parseInt(ii) - parseInt(jj) - 1

                let differenceOfDigits = num1[num1.length - 1 - ii] - num2[num2.length - 1 - jj]

                let newDigitAndCarryOver = (differenceOfDigits + carryOver + (parseInt(solution[currentDigitIndex] || 0))).toString().split('').reverse()
                let newDigit = newDigitAndCarryOver[0]
                carryOver = parseInt(newDigitAndCarryOver[1] || 0)

                solution[currentDigitIndex] = newDigit;
            }
        }

        if (carryOver > 0) solution.unshift(carryOver + '')

        this.value = solution.join('');

        return this
    }

    times(numStr) {
        let carryOver = 0;

        let num1 = numStr.split("")
        let num2 = this.value.split("")

        let solution = new Array(num1.length + num2.length)

        for (let ii in num1) {
            for (let jj in num2) {
                let currentDigitIndex = solution.length - parseInt(ii) - parseInt(jj) - 1

                let productOfDigits = num1[num1.length - 1 - ii] * num2[num2.length - 1 - jj]

                let newDigitAndCarryOver = (productOfDigits + carryOver + (parseInt(solution[currentDigitIndex] || 0))).toString().split('').reverse()
                let newDigit = newDigitAndCarryOver[0]
                carryOver = parseInt(newDigitAndCarryOver[1] || 0)

                solution[currentDigitIndex] = newDigit;
            }
        }

        if (carryOver > 0) solution.unshift(carryOver + '')

        this.value = solution.join('');

        return this
    }

    divideBy(numStr) {
        return this
    }

    equals = () => this.value

    
}


exports.StringifiedNumberCalculator = StringifiedNumberCalculator